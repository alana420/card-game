import pyCardDeck
import pyCardDeck.cards
import argparse
import sys
import pandas as pd
import sqlite3
import logging
import traceback


pd.options.display.max_columns = None

queen_deck = pyCardDeck.Deck(cards=[], name='Queens')
my_deck = pyCardDeck.Deck(cards=[], name='My Deck')
my_hand = pyCardDeck.Deck(cards=[], name='My Hand')
my_temp = pyCardDeck.Deck(cards=[], name='My Temp')
MARKETPLACE = None
QUEENS = None
CARD_HAND_LIMIT = 5
CONNECTION = sqlite3.connect('rpdr.db')

class Base_Card(pyCardDeck.cards.BaseCard):
	def __init__(self, name: str, description: str, money_cost: int, energy_cost: int):
		super().__init__(name)
		self.money_cost  = money_cost
		self.energy_cost = energy_cost
		self.description = description

class Queen_Card(Base_Card):

	def __init__(self, name: str, description: str, money_cost: int, energy_cost:int, defense: int, slay: int, level=0):
		super().__init__(name, description, money_cost, energy_cost)
		self.defense = defense
		self.slay    = slay
		self.level   = level

class Coin_Card(Base_Card):

	def __init__(self, name: str, description: str, money_cost: int, energy_cost:int, value: int):
		super().__init__(name, description, money_cost, energy_cost)
		self.value = value

def build_queens():
	global QUEENS
	QUEENS = pd.read_sql(
		'''
		SELECT * FROM queen
		''',
		CONNECTION
	)
	return QUEENS

def build_marketplace():
	global MARKETPLACE
	MARKETPLACE = pd.read_sql(
		'''
		SELECT *
		FROM (
			SELECT
				id,
				name,
				description,
				cost,
				0 value,
				slay,
				defense
			FROM action
			UNION
			select
				(id + 10),
				name,
				'coin' description,
				cost,
				worth value,
				0 slay,
				0 defense
			from coin
		)
		ORDER BY id
		''',
		CONNECTION
	)

def build_starting_deck(deck):
	STARTING_COINS = {
		'bronze': 7
	}
	coins = pd.read_sql(
		'''
		select
			name,
			worth value,
			cost
		from coin
		''',
		CONNECTION
	)

	for coin_name, quantity in STARTING_COINS.items():
		coin_details = coins.loc[coins["name"] == coin_name]
		money = Coin_Card(
			coin_details['name'][0],
			'money',
			coin_details['cost'][0],
			0,
			coin_details['value'][0]
		)
		for i in range(0,quantity):
			deck.add_single(money)

	return deck

def print_hand(my_hand, limit=None):
	if isinstance(my_hand, list):
		cards = my_hand
	else:
		cards = my_hand._cards

	for idx, card in enumerate(cards):
		if limit is None or (idx+1) <= limit:
			print("{}. {} ".format(idx, card.name))

def put_in_discard(discard_deck, from_deck, position):
	if position is None:
		print('ERROR: include a card position in your hand e.g. -p1')

	discard_card = from_deck._cards.pop(position)
	discard_deck.discard(discard_card)
	print(f"Card {discard_card} put in discard pile.")

def save():
	logging.error(traceback.format_exc())
	my_deck.export('JSON', to_file=True, location='my_deck')
	my_hand.export('JSON', to_file=True, location='my_hand')
	print('saved your hand.')

def game_loop():
	while True:
		cmd = input('>>> ')
		class ThrowingArgumentParser(argparse.ArgumentParser):
			def error(self, message):
				save()

		parser = ThrowingArgumentParser(description='Play Queens.')
		parser.add_argument("command", type=str)
		parser.add_argument("-p","--position", type=int)
		parser.add_argument("-n","--name", type=str)

		args = parser.parse_args(cmd.split())

		command  = args.command
		position = args.position
		name     = args.name

		if command == 'draw':
			while (len(my_hand) < CARD_HAND_LIMIT):
				card = my_deck.draw()
				my_hand.add_single(card)

			print_hand(my_hand)

		elif command == "buy":
			if position is None:
				print('ERROR: include the id of the card you want to buy e.g. buy -p2')
				continue

			card_details = MARKETPLACE.loc[MARKETPLACE['id'] == position]

			card = Base_Card(
				card_details['name'].item(),
				card_details['description'].item(),
				card_details['cost'].item(),
				0
			)
			my_deck.discard(card)
			print('Bought "{}" for ${} from boobsforqueens.com'.format(
				card_details['name'].item(),
				card_details['cost'].item()
			))
		elif command == "queens":
			print(QUEENS)

		elif command == "add_queen":
			if position is None:
				print('ERROR: include the id of the queen you want to add e.g. add_queen -p2')
				continue

			card_details = QUEENS.loc[QUEENS['queenid'] == position]

			card = Queen_Card(
				card_details['queenname'].item(),
				card_details['queenname'].item(),
				0,
				0,
				card_details['defense'].item(),
				card_details['slay'].item(),
				level=card_details['level'].item()
			)
			my_deck.add_single(card)

		elif command == "check":
			if position is None:
				print('ERROR: include the number of cards to check e.g. -p2')
				continue
			while (len(my_hand._cards) < CARD_HAND_LIMIT):
				card = my_deck.draw()
				my_temp.append(card)
				print_hand(my_temp)

		elif command == "select":
			if position is None:
				print('ERROR: state which card you would like to select e.g. -p2')
				continue
			my_hand.add_single(my_temp.pop(position))
			print('Returning remaining cards to your deck.')
			for card in my_temp:
				my_deck.add_single(card, 0)

		elif command == 'discard':
			put_in_discard(my_deck, my_hand, position)

		elif command == 'discard_hand':
			for idx in range(0, len(my_hand._cards)):
				put_in_discard(my_deck, my_hand, 0)

		elif command == 'play':
			put_in_discard(my_deck, my_hand, position)

		elif command == 'r':
			if position is None:
				print('ERROR: include a card position in your hand e.g. -p1')

			card = my_hand._cards.pop(position)
			my_temp.add_single(card)

		elif command == 'marketplace':
			print(MARKETPLACE)

		elif command == 'show':
			if position is not None:
				print(repr(my_hand._cards[position]))
			else:
				print_hand(my_hand)

		elif command == 'show_discard':
			print_hand(my_deck._discard_pile)

		elif command == 'show_deck':
			print_hand(my_deck._cards, position)

		elif command == 'scan':
			if position is None:
				print('ERROR: include the number of cards to scan e.g. -p2')
				continue

			print_hand(my_deck._cards, position)

		elif command == 'scan-next':
			print_hand(my_deck._cards, position)

		elif command == 'exit':
			print_hand(sys.exit())

		elif command == 'restore':
			my_hand.load('my_hand', is_file=True)
			my_deck.load('my_deck', is_file=True)

		else:
			print('command not found: {}'.format(command))
			continue
try:
	build_starting_deck(my_deck)
	build_marketplace()
	build_queens()
	game_loop()
except Exception as e:
	save()

import requests
from io import StringIO
import pandas as pd
import sqlite3
import sqlalchemy

SHEETS = {
	'queen' :'https://docs.google.com/spreadsheets/d/e/2PACX-1vQVncZvyUlQ3NzesYsC6385rrZ3h0jFO6c45zrSPWIBevTJxeGgoXL5pcZlKozbFMxvafnQa4mWEPbi/pub?gid=34059921&single=true&output=csv',
	'coin'  :'https://docs.google.com/spreadsheets/d/e/2PACX-1vQVncZvyUlQ3NzesYsC6385rrZ3h0jFO6c45zrSPWIBevTJxeGgoXL5pcZlKozbFMxvafnQa4mWEPbi/pub?gid=2132434604&single=true&output=csv',
	'action':'https://docs.google.com/spreadsheets/d/e/2PACX-1vQVncZvyUlQ3NzesYsC6385rrZ3h0jFO6c45zrSPWIBevTJxeGgoXL5pcZlKozbFMxvafnQa4mWEPbi/pub?gid=1052155020&single=true&output=csv',
}

def get_sql_column_datatypes_from_df(dfparam):
	dtype_dict = {}

	for i,j in zip(dfparam.columns, dfparam.dtypes):
		if "object" in str(j):
			dtype_dict.update({i: 'TEXT'})

		if "datetime" in str(j):
			dtype_dict.update({i: 'TEXT'})

		if "float" in str(j):
			dtype_dict.update({i: 'REAL'})

		if "int" in str(j):
			dtype_dict.update({i: 'INTEGER'})

	return dtype_dict


def initialize_database():
	connection = sqlite3.connect('rpdr.db')

	for sheet_name, url in SHEETS.items():
		response  = requests.get(url)
		data      = response.text
		data_csv  = StringIO(data)
		df        = pd.read_csv(data_csv)
		datatypes = get_sql_column_datatypes_from_df(df)

		df.to_sql(
			sheet_name,
			con=connection,
			index=False,
			if_exists='replace',
			dtype=datatypes
		)

initialize_database()
